import { v4 as uuid } from 'uuid';

export class Product{
    id : uuid
    image:string;
    name: string;
    description:string;
    price: number;
    
}