import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
@Input ('producto') producto:Product;
@Output('onAgregarALaCesta') onAgregarALaCesta = new EventEmitter<Product>();
@Output('onDeletefromBasket') onDeletefromBasket = new EventEmitter<Product>();
@Output('onShowdescription') onShowdescription = new EventEmitter<string>();
@Output('onDeleteProduct') onDeleteProduct = new EventEmitter<Product>();
  constructor() { }

  ngOnInit() {
  }
  

  agregarALaCesta(product : Product){
    this.onAgregarALaCesta.next(product);
  }
  DeletefromBasket(product : Product){
    this.onDeletefromBasket.next(product);
  }
  Showdescription(description : string){
    this.onShowdescription.next(description);
  }
  deleteProduct(product : Product){
    this.onDeleteProduct.next(product);
  }
}
