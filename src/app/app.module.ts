import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { HelloWorldService } from './services/hello-world.service';
import { ShoppingCartService } from './services/shopping-cart.service';
import { ProductService } from './services/product.service';


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [HelloWorldService, ShoppingCartService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
