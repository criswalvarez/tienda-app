import { Component } from '@angular/core';
import { Product } from 'src/app/models/product';
import { v4 as uuid } from 'uuid';
import { HelloWorldService } from './services/hello-world.service';
import { ShoppingCartService } from './services/shopping-cart.service';
import { ProductService } from './services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'products-app';
 

  constructor(private holamundoservice : HelloWorldService, private shoppingCard : ShoppingCartService, private productService : ProductService){

  }

  setProduct(image: string, name: string, description: string, price:number){
    let producto = new Product();
    producto.id = uuid.v4();
    producto.image = image;
    producto.name = name;
    producto.description = description;
    producto.price = +price; // esto significa que pasamos el input text a number
    this.productService.addProduct(producto)
  }

  agregarAlaCesta(product : Product){
    this.shoppingCard.addProduct(product)
  }
  deletefromBasket(product : Product){
    this.shoppingCard.deletefromBasket(product)
  }
  showDescription(description: string){
    alert(description);
  }

  holaMundo(){
    alert(this.holamundoservice.sayHello())
  }

  getNumberProducts(){
    return this.shoppingCard.getNumberOfProducts()
  }

  getTotalProducts(){
    return this.shoppingCard.getTotalOfShoppingCard()
  }

  getProductsFromService(){
    return this.productService.getProduct()
  }

  deleteProduct(product : Product){
    this.productService.deletefromProducts(product)
    this.shoppingCard.deleteAllProducts(product)
  }
}

