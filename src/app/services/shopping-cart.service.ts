import { Injectable } from '@angular/core';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  
  cardProduct = new Array<Product>();

  constructor() { }

  addProduct(product : Product){
    this.cardProduct.push(product)
  }

  deletefromBasket(product : Product){
    let posicion = this.cardProduct.findIndex(o => o.id == product.id)
    if(posicion >= 0){
      this.cardProduct.splice(posicion, 1)
    }
  }

  deleteAllProducts(product : Product){
    let products = this.cardProduct.filter(o => o.id == product.id)
    products.forEach(o => this.deletefromBasket(o))
  }

  getNumberOfProducts(){
    return this.cardProduct.length
  }

  getTotalOfShoppingCard(){
    if(this.cardProduct.length != 0){
      let prices = this.cardProduct.map(o => o.price)
      return prices.reduce((total, value) => total + value)
    }else{
      return 0
    }
  }
}
