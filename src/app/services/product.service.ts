import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product';
import { v4 as uuid } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private Products = new Array<Product>();
  constructor() { }
  addProduct(product: Product): void{
    this.Products.push(product)
  }

  getProduct(){
    return this.Products
  }


  deletefromProducts(product : Product){
    let posicion = this.Products.findIndex(o => o.id == product.id)
    if(posicion >= 0){
      this.Products.splice(posicion, 1)
    }
  }

}
